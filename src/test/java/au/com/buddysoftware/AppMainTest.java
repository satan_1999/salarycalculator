package au.com.buddysoftware;

import org.junit.Test;

/**
 * The class <code>AppTest</code> contains tests for the class
 * <code>{@link AppMain}</code>.
 */
public class AppMainTest {
    /**
     * Run the void main(String[]) method test.
     *
     */
    @Test
    public void testMainExpectNoException() throws Exception {
        String[] args = new String[] {};

        AppMain.main(args);
        // add additional test code here
    }

    @Test
    public void testMainExpectPrintout() {
        String[] args = new String[] {};

        AppMain.main(args);
        // add additional test code here
    }

}