package au.com.buddysoftware.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import au.com.buddysoftware.domain.Employee;
import au.com.buddysoftware.domain.Salary;
import au.com.buddysoftware.helper.SalaryHelper;

/**
 * The class <code>SalaryServiceTest</code> contains tests for the class
 * <code>{@link SalaryService}</code>.
 *
 * @author wallaceau
 */
public class SalaryServiceTest {
    private String filePath;

    /**
     * Perform post-test clean-up.
     */
    @After
    public void tearDown() throws Exception {
        // Add additional tear down code here
    }

    /**
     * Perform pre-test initialization.
     * 
     * @throws SecurityException
     * @throws NoSuchFieldException
     */
    @Before
    public void setUp() throws NoSuchFieldException, SecurityException {
        String path = getClass().getClassLoader().getResource(".").getPath();
        filePath = path + "resource/data.csv";
        Field field = SalaryService.class.getDeclaredField("batchfileLocation");
        field.setAccessible(true);
        field = SalaryService.class.getDeclaredField("employees");
        field.setAccessible(true);
    }

    @Test
    public void testSalaryServiceExpectSalaryServiceInstance() throws Exception {
        SalaryService result = new SalaryService();
        assertNotNull(result);
        // add additional test code here
    }

    @Test
    public void testSalaryService() throws Exception {
        SalaryService salaryService = new SalaryService();
    }

    /**
     * Run the List<Salary> calculateSalary() method test.
     */
    @Test
    public void testCalculateSalaryExpectEmptyResult() throws Exception {
        SalaryService fixture = new SalaryService();

        List<Salary> result = fixture.calculateSalary();

        // add additional test code here
        assertNotNull(result);
        assertEquals(0, result.size());
    }

    /**
     * Run the List<Salary> calculateSalary() method test.
     *
     * @throws Exception
     *
     */
    @Test
    public void testCalculateSalaryExpectReturn1	() throws Exception {
        SalaryService fixture = mockSalaryService();
        List<Salary> result = fixture.calculateSalary();

        // add additional test code here
        assertEquals(1, result.size());
    }

    private SalaryService mockSalaryService() {
        SalaryService fixture = new SalaryService();
        List<Employee> list = new ArrayList<Employee>();
        Employee employee =  Employee.buildEmployee("Ryan,Chen,120000,10%,01/03/2014,31/03/2014");
        list.add(employee);
        Field field;
        try {
            field = SalaryService.class.getDeclaredField("employees");
            field.setAccessible(true);
            field.set(fixture, list);
            
            field = SalaryService.class.getDeclaredField("salaryHelper");
            field.setAccessible(true);
            field.set(fixture, new SalaryHelper());
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return fixture;
    }

    /**
     * Run the void init() method test.
     *
     * @throws Exception
     */
    @Test
    public void testInitExpectSetAllValue() throws Exception {
        SalaryService fixture = new SalaryService();

        fixture.init();
        Field field = SalaryService.class.getDeclaredField("batchfileLocation");
        field.setAccessible(true);
        // add additional test code here
        assertTrue(field.get(fixture).toString().endsWith("resource/data.csv"));
        field = SalaryService.class.getDeclaredField("employees");
        field.setAccessible(true);
        assertNotNull(field.get(fixture));
    }

    /**
     * Run the List<Employee> readEmployeeFromFile(String) method test.
     *
     * @throws Exception
     */
    @Test
    public void testReadEmployeeFromFileexpectloadSomeEmployees() throws Exception {
        SalaryService fixture = new SalaryService();

        List<Employee> result = fixture.readEmployeeFromFile(filePath);

        // add additional test code here
        assertNotNull(result);
        assertTrue(result.size() > 0);
    }

    /**
     * Run the List<Employee> readEmployeeFromFile(String) method test.
     *
     * @throws Exception
     *
     */
    @Test(expected = java.lang.IllegalArgumentException.class)
    public void testReadEmployeeFromFileExepectException() throws Exception {
        SalaryService fixture = new SalaryService();
        String fileName = null;
        List<Employee> result = fixture.readEmployeeFromFile(fileName);
    }
    
    public void testReadEmployeeFromFileShouldNotThrowException() throws Exception {
        SalaryService fixture = new SalaryService();
        String fileName = "";
        List<Employee> result = fixture.readEmployeeFromFile(fileName);
        assertTrue(result.size() == 0);
    }
    

    

}