package au.com.buddysoftware.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The class <code>EmployeeTest</code> contains tests for the class
 * <code>{@link Employee}</code>.
 */
public class EmployeeTest {
    /**
     * Run the Employee() constructor test.
     */
    @Test
    public void testEmployeeExpectCreateInstance() {
        Employee result = new Employee();
        assertNotNull(result);
    }

    /**
     * Run the int getAnnualSalary() method test.
     */
    @Test
    public void testSetGetAnnualSalaryExpect1() {
        Employee fixture = new Employee();
//        Class[] parameterTypes = new Class[1];
//        parameterTypes[0] = Integer.class;
//        setMethodAccessable(fixture, "setAnnualSalary", parameterTypes);
        fixture.setAnnualSalary(1);
        BigDecimal result = fixture.getAnnualSalary();

        assertEquals(1, result.intValue());
    }

    /**
     * Run the String getFirstName() method test.
     */
    @Test
    public void testSetGetFirstNameExpectTom() {
        Employee fixture = new Employee();
        fixture.setFirstName("Tom");
        String result = fixture.getFirstName();
        assertEquals("Tom", result);
    }

    /**
     * Run the String getLastName() method test.
     */
    @Test
    public void testSetGetLastNameExpectTom() throws Exception {
        Employee fixture = new Employee();
        fixture.setLastName("Tom");
        String result = fixture.getLastName();

        assertEquals("Tom", result);
    }

    /**
     * Run the String getPaymentStartDate() method test.
     */
    @Test
    public void testGetPaymentStartDateExpectCorrectDateRange() throws Exception {
        Employee fixture = new Employee();
        fixture.setPayStart("01/03/2014");
        fixture.setPayEnd("31/03/2014");
        String result = fixture.getPaymentStartDate();
        assertEquals("01/03/2014-31/03/2014", result);
    }

    /**
     * Run the double getSuperRate() method test.
     */
    @Test
    public void testSetGetSuperRateExpect0dot11() throws Exception {
        Employee fixture = new Employee();
        fixture.setSuperRate("11%");
        double result = fixture.getSuperRate();
        assertEquals(0.11 ,result ,4);
    }

    /**
     * Run the void setEmployee(String) method test.
     */
    @Test(expected = java.lang.IllegalArgumentException.class)
    public void testSetEmployeeExpectIllegalArgumentException2() throws Exception {
        String line = "a";
        Employee.buildEmployee(line);
    }

    /**
     * Run the void setEmployee(String) method test.
     */
    @Test
    public void testSetEmployeeExpectNoException() throws Exception {
        String line = "Ryan,Chen,120000,10%,01/03/2014,31/03/2014";
        Employee fixture = Employee.buildEmployee(line);
        assertEquals(120000, fixture.getAnnualSalary().intValue());
        assertEquals("Ryan", fixture.getFirstName());
        assertEquals("Chen", fixture.getLastName());
        assertEquals(0.1 , fixture.getSuperRate(),4);
        assertEquals("01/03/2014-31/03/2014", fixture.getPaymentStartDate());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetEmployeeExpectIllegalArgumentException1() {
        String line = "Ryan,Chen,1A0000,10%,01/03/2014,31/03/2014";
        Employee.buildEmployee(line);
    }

    /**
     * Run the void setEmployee(String) method test.
     */
    @Test(expected = java.lang.IllegalArgumentException.class)
    public void testSetEmployeeExpectIllegalArgumentException() {
        String line = "";
        Employee.buildEmployee(line);
    }

    /**
     * Run the void setSuperRate(String) method test.
     */
    @Test
    public void testSetSuperRateExpectEmptyResult() {
        Employee fixture = new Employee();
        fixture.setSuperRate("11");
        assertEquals(0.0 , fixture.getSuperRate(),4);
    }

    /**
     * Run the void setSuperRate(String) method test.
     */
    @Test(expected = NumberFormatException.class)
    public void testSetSuperRateExpectNumberFormatException() {
        Employee fixture = new Employee();
        String superRateStr = "A%";
        fixture.setSuperRate(superRateStr);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testSetSuperRateOver50ShouldThrowException() {
        Employee fixture = new Employee();
        String superRateStr = "58%";
        fixture.setSuperRate(superRateStr);
    }
    
    /**
     * Run the String getPaymentStartDate() method test.
     */
   /* @Test
    public void testGetPaymentStartDateExpect21()
        throws Exception {
        Employee fixture = new Employee();
        fixture.setPayStart("01/03/2014");
        fixture.setPayEnd("31/03/2014");

        int result = fixture.getPaymentStartDate()

        assertEquals(21, result);
    }*/

    /**
     * Perform pre-test initialization.
     */
    @Before
    public void setUp() throws Exception {
        // add additional set up code here
    }

    /**
     * Perform post-test clean-up.
     */
    @After
    public void tearDown() throws Exception {
        // Add additional tear down code here
    }
    

}