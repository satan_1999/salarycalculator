package au.com.buddysoftware.domain;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.text.ParseException;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import au.com.buddysoftware.helper.SalaryHelper;

/**
 * The class <code>SalaryTest</code> contains tests for the class
 * <code>{@link Salary}</code>.
 */
public class SalaryTest {
    Employee employee;
    SalaryHelper helper;
    @Rule
    public ExpectedException exception = ExpectedException.none();

    /**
     * Perform pre-test initialization.
     */
    @Before
    public void setUp() throws Exception {
    	helper = new SalaryHelper();
        employee = Employee.buildEmployee("firstname,lastname,60050,9%,01/03/2014,31/03/2014");
    }

    /**
     * Perform post-test clean-up.
     */
    @After
    public void tearDown() throws Exception {
        // Add additional tear down code here
    }

    /**
     * Run the void setSalary(Employee) method test.
     */
//    @Test
//    public void testsetSalaryExpectNoException() throws Exception {
//        Salary fixture = new Salary();
//        fixture.buildSalary(employee, 5004, 0.23);
//    }

    /**
     * Run the int getGrossIncome() method test.
     */
    @Test
    public void testGetGrossIncomeExpect5004() throws Exception {
        Salary fixture = new Salary();
        fixture.setGrossIncome(5004.01);

        int result = fixture.getGrossIncome().intValue();

        // add additional test code here
        assertEquals(5004, result);
    }

    /**
     * Run the int getIncomeTax() method test.
     */
    @Test
    public void testGetIncomeTaxShouldReturn10() throws Exception {
    	 Salary fixture = new Salary();
    	 fixture.setIncomeTax(10.01);
        assertEquals(10, fixture.getIncomeTax().intValue());
    }

    /**
     * Run the String getName() method test.
     */
    @Test
    public void testGetNameExpectfirstnamelastname() throws Exception {
    	 Salary fixture = new Salary();
    	 fixture.setName("firstname lastname");
        String result = fixture.getName();

        assertEquals("firstname lastname", result);
    }

    /**
     * Run the int getNetIncome() method test.
     */
	@Test
	public void testGetNetIncomeExpectReturn4082() throws Exception {
		Salary fixture = new Salary();
		fixture.setGrossIncome(5082);
		fixture.setIncomeTax(1000);

		BigDecimal result = fixture.getNetIncome();

		// add additional test code here
		assertEquals(4082, result.intValue());
	}

    /**
     * Run the int getSuperannuation() method test.
     */
    @Test
    public void testGetSuperannuationExpect450() throws Exception {
        Salary fixture = new Salary();
        fixture.setSuperannuation(450);

        BigDecimal result = fixture.getSuperannuation();

        // add additional test code here
        assertEquals(450, result.intValue());
    }

    /**
     * Run the void setGrossIncome(double) method test.
     */
    @Test
    public void testSetGrossIncomeExpect10() throws Exception {
        Salary fixture = new Salary();
        fixture.setGrossIncome(10.02);
        assertEquals(10 , fixture.getGrossIncome().intValue());
    }

    @Test
    public void testSetGrossIncomeExpect11() throws Exception {
        Salary fixture = new Salary();
        fixture.setGrossIncome(10.52);
        assertEquals(11 , fixture.getGrossIncome().intValue());
    }

    /**
     * Run the void setIncomeTax(int) method test.
     */
    @Test
    public void testSetIncomeTaxExpect10() throws Exception {
        Salary fixture = new Salary();
        fixture.setIncomeTax(10.02);
        assertEquals(10 , fixture.getIncomeTax().intValue());
    }

    @Test
    public void testSetIncomeTaxExpect11() throws Exception {
        Salary fixture = new Salary();
        fixture.setIncomeTax(10.62);
        assertEquals(11 , fixture.getIncomeTax().intValue());
    }

    /**
     * Run the void setSuperannuation(double) method test.
     */
    @Test
    public void testSetSuperannuationExpect10() throws Exception {
        Salary fixture = new Salary();
        fixture.setSuperannuation(10.22);

        assertEquals(10 , fixture.getSuperannuation().intValue());
    }

    @Test
    public void testSetSuperannuationExpect11() throws Exception {
        Salary fixture = new Salary();
        fixture.setSuperannuation(10.52);

        assertEquals(11 , fixture.getSuperannuation().intValue());
    }

    /**
     * Run the String toString() method test.
     */
    @Test
    public void testToStringExpectString() throws Exception {
    	Salary fixture = new Salary();
    	fixture.setGrossIncome(5004.0).setName("firstname lastname").setPayPeriod("01/03/2014-31/03/2014")
    	.setIncomeTax(922.0).setSuperannuation(450.0);

        String result = fixture.toString();

        // add additional test code here
        assertEquals("firstname lastname,01/03/2014-31/03/2014,5004,922,4082,450", result);
    }

    @Test
    public void testValidatePaymentPeriodNotInOneMonthShouldFailValidationThrowException() throws Exception  {
    	 Employee employee = new Employee();
    	 employee.setPayStart("01/02/2014");
    	 employee.setPayEnd("31/03/2014");
    	 
    	 exception.expect(IllegalArgumentException.class);
    	 exception.expectMessage("Can not calculator salary for "
					+ employee.getFirstName()
					+ " "
					+ employee.getLastName()
					+ " , due to Payment Payment period ["+employee.getPayStart()+" - "+employee.getPayEnd()+"] must be within same month and year");
    	
         Salary fixture = new Salary();
         fixture.validatePaymentPeriod(employee);
    }
    
    @Test
    public void testValidatePaymentPeriodShouldFailValidationThrowException() throws Exception  {
    	 Employee employee = new Employee();
    	 employee.setPayStart("01/02/2012");
    	 employee.setPayEnd("20/02/2012");
    	 
    	 exception.expect(IllegalArgumentException.class);
    	 exception.expectMessage("Can not calculator salary for null null , due to Payment period [Wed Feb 01 00:00:00 EST 2012 - Mon Feb 20 00:00:00 EST 2012] must be after 1 July 2012");

         Salary fixture = new Salary();
         fixture.validatePaymentPeriod(employee);
    }
    
    @Test
    public void testValidatePaymentPeriodNotInOneYearShouldFailValidationThrowException() throws Exception  {
    	 Employee employee = new Employee();
    	 employee.setPayStart("01/02/2014");
    	 employee.setPayEnd("20/02/2015");
    	 
    	 exception.expect(IllegalArgumentException.class);
    	 exception.expectMessage("Can not calculator salary for null null , due to Payment period [Wed Feb 01 00:00:00 EST 2014 - Mon Feb 20 00:00:00 EST 2015] must be within same month and year");

         Salary fixture = new Salary();
         fixture.validatePaymentPeriod(employee);
    }



}