/**
 * 
 */
package au.com.buddysoftware.helper;

import static org.junit.Assert.assertEquals;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import au.com.buddysoftware.domain.Employee;

/**
 * @author wallaceau
 *
 */
public class SalaryHelperTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
    public void testGetWorkedMonthsShouldReturn2() {
    	Employee employee = Employee.buildEmployee("Ryan,Chen,120000,10%,30/03/2014,02/06/2014");
        SalaryHelper fixture = new SalaryHelper();
        int result = fixture.getWorkedMonths(employee);
        assertEquals(2, result);
    }

    @Test
    public void testGetWorkedMonthsShouldReturn1() {
    	Employee employee = Employee.buildEmployee("Ryan,Chen,120000,10%,01/03/2014,31/03/2014");
        SalaryHelper fixture = new SalaryHelper();
        int result = fixture.getWorkedMonths(employee);
        assertEquals(1, result);
    }

//    @Test
//    public void testGetExtraWorkedDaysShouldReturn2() {
//    	Employee employee = Employee.buildEmployee("Ryan,Chen,120000,10%,30/03/2014,02/06/2014");
//        SalaryHelper fixture = new SalaryHelper();
//        int result = fixture.getExtraWorkedDays(employee);
//        assertEquals(2, result);
//    }

    @Test
    public void testGetWorkingWeekDaysShouldReturn1() {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date date1 = df.parse("30/03/2014");
            Date date2 = df.parse("31/03/2014");
            SalaryHelper fixture = new SalaryHelper();
            int result = fixture.getWorkingWeekDays(date1, date2);
            assertEquals(1, result);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    public void testGetWorkingWeekDaysShouldReturn17() {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date date1 = df.parse("01/03/2014");
            Date date2 = df.parse("25/03/2014");
            SalaryHelper fixture = new SalaryHelper();
            int result = fixture.getWorkingWeekDays(date1, date2);
            assertEquals(17, result);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    public void testgetTaxRateShouldReturndot20424285714285714() throws Exception {
        Employee employee = Employee.buildEmployee("Ryan,Chen,70000,9%,30/03/2014,02/06/2014");
        SalaryHelper fixture = new SalaryHelper();
        double result = fixture.getTaxRate(employee);

        // add additional test code here
        assertEquals( 0.20424285714285714,  result, 4);
    }

    @Test
    public void testgetTaxRateShouldReturndot2360() throws Exception {
    	Employee employee = Employee.buildEmployee("Ryan,Chen,90000,9%,30/03/2014,02/06/2014");
        SalaryHelper fixture = new SalaryHelper();
        double result = fixture.getTaxRate(employee);

        // add additional test code here
        assertEquals( 0.2360, result, 4);
    }

    @Test
    public void testgetTaxRateShouldReturndot3520() throws Exception {
    	Employee employee = Employee.buildEmployee("Ryan,Chen,270000,9%,30/03/2014,02/06/2014");
        SalaryHelper fixture = new SalaryHelper();
        double result = fixture.getTaxRate(employee);

        // add additional test code here
        assertEquals(0.3520, result, 4);
    }

    @Test
    public void testgetTaxRateShouldReturn0() throws Exception {
    	Employee employee = Employee.buildEmployee("Ryan,Chen,200,9%,30/03/2014,02/06/2014");
        SalaryHelper fixture = new SalaryHelper();
        double result = fixture.getTaxRate(employee);

        // add additional test code here
        assertEquals(0.0, result, 4);
    }

    @Test
    public void testgetTaxRateShouldReturndot0099() throws Exception {
    	Employee employee = Employee.buildEmployee("Ryan,Chen,19200,9%,30/03/2014,02/06/2014");
        SalaryHelper fixture = new SalaryHelper();
        double result = fixture.getTaxRate(employee);

        // add additional test code here
        assertEquals(0.0099, result, 4);
    }

    @Test
    public void testCalculateGrossPayShouldReturn3926() throws Exception {
    	Employee employee = Employee.buildEmployee("Ryan,Chen,60050,9%,01/03/2014,25/03/2014");
        SalaryHelper fixture = new SalaryHelper();
        SalaryHelper spy = Mockito.spy(fixture);
      
        Mockito.doReturn(0).when(spy).getWorkingWeekDays(Mockito.any(Date.class), Mockito.any(Date.class));
        double result = fixture.calculateGrossPay(employee);
        assertEquals(3926.0, result, 4);
    }

}
