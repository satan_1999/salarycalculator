package au.com.buddysoftware.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import au.com.buddysoftware.domain.Employee;
import au.com.buddysoftware.domain.Salary;
import au.com.buddysoftware.helper.SalaryHelper;

@Service
public class SalaryService {

    
    protected static Logger log = Logger.getLogger(SalaryService.class);
    private String batchfileLocation;
    private List<Employee> employees;
    SalaryHelper salaryHelper;

    /*
     * Load employees information from file
     */
    public void init() {
        log.debug("Calling init");
        log.setLevel(Level.DEBUG);
        salaryHelper = new SalaryHelper();
        String path = getClass().getClassLoader().getResource(".").getPath();
        batchfileLocation = path + "resource/data.csv";
        // load employees information from file
        employees = readEmployeeFromFile(batchfileLocation);
    }

    /**
     * calculate salary for all employee
     */
	public List<Salary> calculateSalary() {
		log.debug("Calling calculate");
		List<Salary> salaries = new ArrayList<Salary>();
		if(employees == null || employees.isEmpty())
		{
			return salaries;
		}
		
		for (Employee employee : employees) {
			try {
				// calculate salary for each employee
				Salary salary = salaryHelper.buildSalary(employee);
				salaries.add(salary);
			} catch (Exception ex) {
				log.error("Failed to calculateSalary", ex);
			}
		}
		return salaries;
	}

    /**
     * Read Employee information from file and create Employee domain class
     * 
     * @param fileName
     * @return
     * @throws IOException
     */
    public List<Employee> readEmployeeFromFile(String fileName) {
        Assert.notNull(fileName, "The fileName must not be null");
        List<Employee> employees = new ArrayList<Employee>();
        BufferedReader in = null;

        try {
            File file = new File(fileName);
            in = new BufferedReader(new FileReader(file));

            String line;
            while ((line = in.readLine()) != null) {
                // populate Employee domain
                employees.add(Employee.buildEmployee(line));
            }
            log.info("Loaded Employees  " + employees.size());
            in.close();
        } catch (IOException e) {
            log.error("Failed to load file", e);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    log.error("Failed to close file", e);
                }
            }
        }
        return employees;
    }



}
