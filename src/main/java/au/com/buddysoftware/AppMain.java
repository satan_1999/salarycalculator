package au.com.buddysoftware;

import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import au.com.buddysoftware.domain.Salary;
import au.com.buddysoftware.service.SalaryService;


/**
 * Hello world!
 *
 */
public class AppMain {
    /**
     * Start to run the program
     * 
     * @param args
     */
    public static void main(final String[] args) {
        BasicConfigurator.configure(); 
        // Create Service SalaryService
        ApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "resource/applicationContext.xml" });
        SalaryService salaryService = (SalaryService) context.getBean("salaryService");
        // load employees from file, init() can be called when spring creates the bean
        salaryService.init();
        // calculate salary for each employee
        List<Salary> salarys = salaryService.calculateSalary();
        // print out the salary
        for (Salary salary : salarys) {
            System.out.println(salary.toString());
        }
    }
}
