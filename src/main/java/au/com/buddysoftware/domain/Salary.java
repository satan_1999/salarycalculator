package au.com.buddysoftware.domain;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Date;
import java.util.UUID;

import org.joda.time.DateTime;
import org.springframework.util.Assert;

public class Salary {

	private String refKey = UUID.randomUUID().toString();
	private String name;
	private String payPeriod;
	private BigDecimal grossIncome;
	private BigDecimal incomeTax;
	private BigDecimal superannuation;
	private Date payStart;
	private Date payEnd;	

	/*
	 * Rule 1. Payment start and end day must be within same month and same year
	 * Rule 2. Date must be after 1 July 2012
	 * @param employee
	 * @return
	 */
	public Salary validatePaymentPeriod(Employee employee) {
		
		DateTime start = new DateTime(employee.getPayStart());
		DateTime end = new DateTime(employee.getPayEnd());
		DateTime firstJuly2012 = new DateTime(2012, 7, 1, 0, 0, 0);
		// Rule 1:
		Assert.isTrue((start.getMonthOfYear() == end.getMonthOfYear())&&(start.getYear() == end.getYear()),
				"Can not calculator salary for "
						+ employee.getFirstName()
						+ " "
						+ employee.getLastName()
						+ " , due to Payment Payment period ["+employee.getPayStart()+" - "+employee.getPayEnd()+"] must be within same month and year");
		// Rule 2:
		Assert.isTrue(employee.getPayStart().compareTo(firstJuly2012.toDate()) >= 0,
				"Can not calculator salary for "
						+ employee.getFirstName()
						+ " "
						+ employee.getLastName()
						+ " , due to Payment period ["+employee.getPayStart()+" - "+employee.getPayEnd()+"] must be after 1 July 2012");

		return this;
	}

	

	public String getName() {
		return name;
	}

	public String getPayPeriod() {
		return payPeriod;
	}

	public BigDecimal getGrossIncome() {
		return grossIncome;
	}

	public BigDecimal getIncomeTax() {
		return incomeTax;
	}

	public BigDecimal getNetIncome() {
		// return this.grossIncome - this.incomeTax;
		return grossIncome.subtract(incomeTax, new MathContext(0));
	}

	/*
	 * public void setNetIncome(int netIncome) { this.netIncome = netIncome; }
	 */
	public BigDecimal getSuperannuation() {
		return superannuation;
	}

	public String toString() {
		return this.getName() + "," + this.getPayPeriod() + ","
				+ this.getGrossIncome() + "," + this.getIncomeTax() + ","
				+ this.getNetIncome() + "," + this.getSuperannuation();
	}

	public Salary setName(String name) {
		this.name = name;
		return this;
	}

	public Salary setPayPeriod(String payPeriod) {
		this.payPeriod = payPeriod;
		return this;
	}

	public Salary setSuperannuation(double superannuation) {
		this.superannuation = new BigDecimal(superannuation).setScale(0,
				RoundingMode.HALF_UP);
		return this;
	}

	public Salary setIncomeTax(double tax) {
		this.incomeTax = new BigDecimal(tax).setScale(0,
				RoundingMode.HALF_UP);
		return this;
	}

	public Salary setGrossIncome(double grossIncome) {
		this.grossIncome = new BigDecimal(grossIncome).setScale(0,
				RoundingMode.HALF_UP);
		return this;
	}

}
