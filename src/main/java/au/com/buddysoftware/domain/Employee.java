package au.com.buddysoftware.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

public class Employee {
    private static Logger log = Logger.getLogger(Employee.class);
    private static final char LINE_SEPERATOR = ',';
    private static final String PERCENTAGE_SIGN = "%";
    private static final String DATE_FORMAT = "dd/MM/yyyy";

    private String refKey = UUID.randomUUID().toString();
    private String firstName;
    private String lastName;
    private BigDecimal annualSalary;
    private double superRate;
    private Date payStart;
    private Date payEnd;

    public static Employee buildEmployee(String line) {

        String[] splitLine = StringUtils.split(line, LINE_SEPERATOR);
        Assert.isTrue(splitLine.length == 6, "This line is not correct");
        try {
        	Employee employee = new Employee()
        	.setFirstName(splitLine[0])
        	.setLastName(splitLine[1])
        	.setAnnualSalary(Integer.parseInt(splitLine[2]))
        	.setSuperRate(splitLine[3])
        	.setPayStart(splitLine[4])
        	.setPayEnd(splitLine[5]);
        	
        	return employee;
        } catch (Exception ex) {
            log.error("Failed to setEmployee", ex);
            throw new IllegalArgumentException("This line is not correct");
        }
    }
    
    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getPayStart() {
        return this.payStart;
    }

    public Date getPayEnd() {
        return this.payEnd;
    }

    public BigDecimal getAnnualSalary() {
        return annualSalary;
    }
   

    public double getSuperRate() {
        return superRate;
    }

    public String getPaymentStartDate() {
        DateFormat df = new SimpleDateFormat(DATE_FORMAT);
        return df.format(this.payStart) + "-" + df.format(this.payEnd);
    }

    /*
     * public void setPaymentStartDate(String paymentStartDate) {
     * this.paymentStartDate = paymentStartDate; }
     */
    Employee setPayStart(String payStartStr) throws ParseException {
        DateFormat df = new SimpleDateFormat(DATE_FORMAT);
        this.payStart = df.parse(payStartStr);
        return this;
    }

    Employee setPayEnd(String payEndStr) throws ParseException {
        DateFormat df = new SimpleDateFormat(DATE_FORMAT);
        this.payEnd = df.parse(payEndStr);
        return this;
    }

   
    Employee setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }
   
    Employee setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }
    
    Employee setAnnualSalary(int annualSalary) {
        this.annualSalary = new BigDecimal(annualSalary).setScale(0, RoundingMode.HALF_UP);
        return this;
    }
    
    
    Employee setSuperRate(String superRateStr) {
        if (superRateStr != null && superRateStr.endsWith(PERCENTAGE_SIGN)) {
            String superNumber = superRateStr.replaceAll(PERCENTAGE_SIGN, "");
            this.superRate = Double.parseDouble(superNumber) / 100;
            Assert.isTrue(( this.superRate >0 && this.superRate <= 0.5));
        }
        return this;
    }
}
