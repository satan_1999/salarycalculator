package au.com.buddysoftware.helper;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Months;
import org.springframework.util.Assert;

import au.com.buddysoftware.domain.Employee;
import au.com.buddysoftware.domain.Salary;

public class SalaryHelper {
	private static final int NUMBER_WEEKDAY_PER_YEAR = 260;
    private static final int NUMBER_MONETH_PER_YEAR = 12;
    private static final char LINE_SEPERATOR = ',';
    protected static Logger log = Logger.getLogger(SalaryHelper.class);
    
    public Salary buildSalary(Employee employee) {
    	
    	Salary salary = new Salary();
    	 salary.validatePaymentPeriod(employee)
    	.setName(employee.getFirstName() + " " + employee.getLastName())
    	.setGrossIncome(calculateGrossPay(employee))
    	.setIncomeTax( salary.getGrossIncome().doubleValue() * getTaxRate(employee))
    	.setSuperannuation(salary.getGrossIncome().doubleValue() * employee.getSuperRate())
    	.setPayPeriod(employee.getPaymentStartDate());
    	
    	return salary;
    }  
    
    public double calculateGrossPay(Employee employee) {
        int days = 0;
        // get the number of full months during pay period
        int months = getWorkedMonths(employee);
        Assert.isTrue(months <=1, "The number of worked month ["+employee.getPayStart()+""+employee.getPayEnd()+"] is wrong");
        
        double grossSalary = 0.0;
        if (months < 1) {
            // less than one month , pay = get number of worked days * daily rate
            days = getWorkingWeekDays(employee.getPayStart(), employee.getPayEnd());
            grossSalary = days * (employee.getAnnualSalary().doubleValue() / NUMBER_WEEKDAY_PER_YEAR);
        } else {
            // one full month pay
            grossSalary = employee.getAnnualSalary().doubleValue() / NUMBER_MONETH_PER_YEAR ;
        }
        return grossSalary;
    }

    /**
     * Calculate how many full months during pay period
     * 
     * @param employee
     * @return
     */
    public int getWorkedMonths(Employee employee) {

        DateTime start = new DateTime(employee.getPayStart());
        DateTime end = new DateTime(employee.getPayEnd());
       
        DateTime firstDayOfMonth = start.dayOfMonth().withMinimumValue();
        DateTime lastDayOfMonth = end.dayOfMonth().withMaximumValue();

        if (!start.isEqual(firstDayOfMonth)) {
            // set the start date to first of next month
            start = start.plusMonths(1);
            start = start.dayOfMonth().withMinimumValue();
        }

        if (!end.isEqual(lastDayOfMonth)) {
            // set the end date to first of the month
            end = end.dayOfMonth().withMinimumValue();
        }
        return Months.monthsBetween(start, end.plusDays(1)).getMonths();

    }

    //FIXME : only have tax rate for pay period after 1 July 2012
    public double getTaxRate(Employee employee) {
        int annualSalary = employee.getAnnualSalary().intValue();
        double incomeTax = 0;
        if (0 <= annualSalary && annualSalary <= 18200) {
            incomeTax = 0;
        } else if (18201 <= annualSalary && annualSalary <= 37000) {
            incomeTax = (annualSalary - 18200) * 0.19;
        } else if (37001 <= annualSalary && annualSalary <= 80000) {
            incomeTax = (annualSalary - 37000) * 0.325 + 3572;
        } else if (80001 <= annualSalary && annualSalary <= 180000) {
            incomeTax = (annualSalary - 80000) * 0.37 + 17547;
        } else if (180001 <= annualSalary) {
            incomeTax = (annualSalary - 180000) * 0.45 + 54547;
        }
        return incomeTax / annualSalary;

    }

    public int getWorkingWeekDays(Date start, Date end) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(start);
        cal2.setTime(end);

        int numberOfDays = 0;
        while (!cal1.after(cal2)) {
            if ((Calendar.SATURDAY != cal1.get(Calendar.DAY_OF_WEEK)) && (Calendar.SUNDAY != cal1.get(Calendar.DAY_OF_WEEK))) {
                numberOfDays++;
                cal1.add(Calendar.DATE, 1);
            } else {
                cal1.add(Calendar.DATE, 1);
            }
        }
        return numberOfDays;
    }
}
